import numpy as np

num1 = 0
num2 = 0

print("Bienvenido a tu calculadora")


menu = """
1.- Suma
2.- Restar
3.- Multiplicar
4.- Dividir
5.- Raiz
6.- Exponente
7.- Seno
8.- Coseno
9.- Tangente
10.- Salir

"""


def Suma():
    print("Ingresa el primer número: ")
    num1 = float(input("Ingresa el primer número: "))
    print("Ingresa el segundo número: ")
    num2 = float(input())
    print("El resultado de la Suma es: ", num1 + num2)

def Resta():
    print("Ingresa el primer número: ")
    num1 = float(input())
    print("Ingresa el segundo número: ")
    num2 = float(input())
    print("El resultado de tu Resta es: ", num1 - num2)

def Multiplcacion():
    print("Ingresa el primer número: ")
    num1 = float(input())
    print("Ingresa el segundo número: ")
    num2 = float(input())
    print("El resultado de tu Resta es: ", num1 * num2)

def Dividición():
    print("Ingresa el primer número: ")
    num1 = float(input())
    print("Ingresa el segundo número: ")
    num2 = float(input())
    print("El resultado de tu Resta es: ", num1 / num2)

def Raiz():
    print("Ingresa el primer número: ")
    num1 = float(input())
    print("El resultado de tu Raiz es: ", np.sqrt(num1))

def Exponente():
    print("Ingresa el primer número: ")
    num1 = float(input())
    print("El resultado de tu Exponente es: ", np.exp(num1))

def Seno():
    print("Ingresa un numero para calcular el Seno: ")
    num1 = float(input())
    print("El resultado del Seno es: ", (np.sin(np.radians(num1))))

def Coseno():
    print("Ingresa un numero para calcular el Coseno: ")
    num1 = float(input())
    print("El resultado del Coseno es: ", (np.cos(np.radians(num1))))

def Tangente():
    print("Ingresa un numero para calcular la Tangente: ")
    num1 = float(input())
    print("El resultado de la Tangente es: ", (np.tan(np.radians(num1))))

    print("Selecciona una opcion")
while True:
    print(menu)
    opc = int(input())
    if opc == 1:
        Suma()
    elif opc == 2:
        Resta()
    elif opc == 3:
        Multiplcacion()
    elif opc == 4:
        Dividición()
    elif opc == 5:
        Raiz()
    elif opc == 6:
        Exponente()
    elif opc == 7:
        Seno()
    elif opc == 8:
        Coseno()
    elif opc == 9:
        Tangente()
    elif opc == 10:
        keep_running = input("Deseas continuar? Si o No?")
        if keep_running in ('SI', 'S', 's', 'Yes', 'y', 'yes', 'Y', 'YES'):
            continue
        else:
            break